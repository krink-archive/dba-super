#!/usr/bin/env python


dbconf  = 'dba.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

import sys
host = sys.argv[1]

config = {
  'user': dbaUser,
  'password': dbaPass,
}



#MYSQL_USER=$dbaUser
#MYSQL_PASS=$dbaPass
#MYSQL_CONN="-u${MYSQL_USER} -p${MYSQL_PASS}"
#MYSQLDUMP_OPTIONS=" --master-data=2 --single-transaction --quick "
#mysqldump ${MYSQL_CONN} ${MYSQLDUMP_OPTIONS} --all-databases   > mysqldump.all-databases.`date --iso`.sql

cmd = 'ssh -o LogLevel=Error -n -f ' + str(host) + ' '
cmd += '"sh -c \'nohup mysqldump --socket=/var/lib/mysql/mysql.sock -u' + str(dbaUser) + ' -p' + str(dbaPass) 
cmd += ' --master-data=2 --single-transaction --quick --all-databases  > /data/mysqldump/mysqldump.all-databases.sql 2>&1 &\'"'

#cmd = 'ssh -t -o LogLevel=Error ' + str(host) + ' '
#cmd = 'nohup mysqldump --socket=/var/lib/mysql/mysql.sock '
#cmd += ' -u' + str(dbaUser) + ' -p' + str(dbaPass) + ' '
#cmd += ' --master-data=2 --single-transaction --quick '
#cmd += ' --all-databases > /data/mysqldump/mysqldump.all-databases.sql &'
import subprocess
subcmd = subprocess.call(cmd, shell=True)
#subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
#output = subcmd.communicate()
#exitcode = subcmd.wait()
#if (exitcode != 0):
#    print 'Error:  ' + str(exitcode) + ' ' + str(host)

print 'Launched mysqldump on ' + str(host)
#print ''.join(output[0])

