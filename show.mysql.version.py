#!/bin/env python

__version__ = 'show.mysql.version.1'

dbconf  = 'dba.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]


import sys
host = sys.argv[1]

try:
    port = sys.argv[2]
except IndexError:
    port = '3306'

config = {
  'user': dbaUser,
  'password': dbaPass,
  #'unix_socket': '/var/lib/mysql/mysql.sock',
  'host' : host,
  'port' : port,
  'database': 'mysql',
  'raise_on_warnings': True,
}


try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)

try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor(buffered=True)
try:
  sql = "select version();"
  cursor.execute(sql)
  select_version = cursor.fetchone()

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()

print str(select_version)

sys.exit(0)
