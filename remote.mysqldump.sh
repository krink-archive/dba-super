
host="$1"

conf="dba.conf"
#define('dbaUser','user');
#define('dbaPass','pass');

dbaUser=$(grep ^define $conf | grep dbaUser | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
dbaPass=$(grep ^define $conf | grep dbaPass | awk -F"," '{print $2}' | awk -F"'" '{print $2}')


ssh -n -f "$1" "sh -c 'nohup mysqldump --socket=/var/lib/mysql/mysql.sock -u$dbaUser -p$dbaPass --master-data=2 --single-transaction --quick --all-databases  > /data/mysqldump/mysqldump.all-databases.sql 2>&1 &'"


