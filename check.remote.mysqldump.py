#!/usr/bin/env python

import sys
host = sys.argv[1]


dumpfile='/data/mysqldump/mysqldump.all-databases.sql'

import subprocess
def runCmd(cmdline=None):
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
    output = subcmd.communicate()
    exitcode = subcmd.wait()
    if (exitcode != 0):
         print 'Error:  ' + str(exitcode) + ' ' + str(host) + ' ' + str(output)
         sys.exit(exitcode)
    return ''.join(output[0])

cmd = 'tail -1 ' + dumpfile
cmdline = 'ssh -t -o LogLevel=Error ' + str(host) + ' ' + str(cmd)
line = runCmd(cmdline)
if line.startswith('-- Dump completed on '):
    print 'Done ' + str(line)
    sys.exit(0)
else:
    print 'Not ready ' + str(dumpfile)
    # more checking...
    #cmd = 'ls -al ' + str(dumpfile) + ' | awk \'{print $5, $6, $7, $8, $9}\''
    cmd = 'ls -al ' + str(dumpfile) + ' | awk \'{print $5, $6, $7, $8}\''
    cmd += '; ps -ef | grep mysqldump | grep -v grep | awk \'{print $2, $8}\''
    cmd += '; df -h ' + str(dumpfile)
    cmdline = 'ssh -t -o LogLevel=Error ' + str(host) + ' ' + str(cmd)
    out = runCmd(cmdline)
    print out
    sys.exit(99)




