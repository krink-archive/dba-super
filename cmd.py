#!/usr/bin/env python

__version__ = '0.0.0.0.1.2.3.4.10'

import sys
sys.dont_write_bytecode = True

def usage():
    #./cmd.py host cmd option1 option2
    print 'python ' + sys.argv[0] + ' host cmd option1 option2'
    print """\

        host in [type-datacenter-number|type-datacenter|type]
        host [ssh-key]

    """
    return True

import subprocess
import re

def sshCmd(host=None,cmd=None):
    #cmdline = 'ssh -o LogLevel=Error -i .ssh/id_rsa  ' + host + ' ' + cmd
    cmdline = 'ssh -t -o LogLevel=Error ' + host + ' ' + cmd
    #subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #out, err = subcmd.communicate()
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
    output = subcmd.communicate()
    exitcode = subcmd.wait()
    if (exitcode != 0):
        print 'Error:  ' + str(exitcode) + ' ' + str(host)
    print host
    print ''.join(output[0])

def sudosshCmd(host=None,cmd=None):

    import os
    pfile = '.pass'
    if os.path.isfile(pfile):
        with open(pfile, "r") as passfile:
            sudopass = passfile.readline()
        print "Using password file " + str(pfile)
    else:
        #sudopass = sys.stdin.readline()
        import getpass
        sudopass = getpass.getpass('Sudo Pass:')
        
    cmdline = 'ssh -t -o LogLevel=Error ' + str(host) + ' echo ' + str(sudopass) + ' | sudo -S ' + cmd + ' '
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
    output = subcmd.communicate()
    exitcode = subcmd.wait()
    if (exitcode != 0):
        print 'Error:  ' + str(exitcode) + ' ' + str(host)
    print host
    print ''.join(output[0])


def lookup_hostname(hostname=None):

    hostfile = '/etc/hosts'
    hostList = []
    with open(hostfile, "r") as infile:
        data = infile.read()

    dataList = data.splitlines()
    #print type(dataList)

    lnum = 0
    lineDict = {}
    for line in dataList:
        if line.startswith("#"):
            continue
        lnum += 1
        lineDict[lnum] = line

    #print str(lineDict)
    for key in lineDict:
        line = lineDict[key].split()
        if hostname in line:
            #print hostname
            hostList.append(hostname)
            return hostList

    longList = []
    for key in lineDict:
        line = lineDict[key].split()
        longList.extend(line)

    for item in longList:
        pattern = hostname + '.*'
        m = re.match(pattern, item)
        if m:
            hostList.append(item)

    return hostList

def remove_non_host(hostList=[]):
    """ remove vips, etc... """

    pattern = '-vip'
    for host in hostList:
        if pattern in host:
            #print 'remove ' + host
            hostList.remove(host)

    return hostList


def run_cmd(hostname='', cmd=[], sudo=False):
    """ run command """
    #print 'cmd: ' + str(cmd)
    if not cmd:
        cmdline = 'uptime'
    else:
        cmdline = ' '.join(cmd)

    hostList = lookup_hostname(hostname)
    hostList = remove_non_host(hostList)

    for host in hostList:
        if sudo:
            sudosshCmd(host,cmdline)
        else:
            sshCmd(host,cmdline)

    return True

def ssh_key_setup(hostname=''):
    hostList = lookup_hostname(hostname)
    hostList = remove_non_host(hostList)

    import os
    for host in hostList:
        print host
        cmdset = 'cat ~/.ssh/id_rsa.pub | ssh %s "mkdir -p ~/.ssh;chcon -R -t ssh_home_t ~/.ssh;cat >> ~/.ssh/authorized_keys2"'  % (host)
        os.system(cmdset)

    return True


if __name__ == "__main__":
    """ run main """

    options = {
      'run-cmd' : run_cmd,
      'ssh-key' : ssh_key_setup,
    }

    if sys.argv[1:]:
        if len(sys.argv) == 2:
            options['run-cmd'](hostname=sys.argv[1], cmd=['uptime'])
            sys.exit(0)
        if sys.argv[2] == "sudo":
            options['run-cmd'](hostname=sys.argv[1], cmd=sys.argv[3:], sudo=True)
            sys.exit(0)
        if sys.argv[2] == "ssh-key":
            options['ssh-key'](hostname=sys.argv[1])
            sys.exit(0)
        else:
            options['run-cmd'](hostname=sys.argv[1], cmd=sys.argv[2:])
            sys.exit(0)
    else:
        usage()
        sys.exit(0)

sys.exit(0)
