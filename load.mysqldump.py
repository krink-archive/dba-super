#!/usr/bin/env python

import sys
src = sys.argv[1]
dest = sys.argv[2]
port = 3308

dumpfile='/data/mysqldump/mysqldump.all-databases.sql'

dbconf  = 'dba.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
}


import subprocess
def runCmd(cmdline=None):
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
    output = subcmd.communicate()
    exitcode = subcmd.wait()
    if (exitcode != 0):
         print 'Error:  ' + str(exitcode) + ' ' + str(output)
         sys.exit(exitcode)
    return ''.join(output[0])

#mysql -u krink  -p -h 192.168.248.186 -P 3308 < mysqldump.all-databases.2017-08-14.sql
cmd = 'mysql -u' + str(dbaUser)  + ' -p' + str(dbaPass) + ' -h ' + str(dest)
cmd += ' -P ' + str(port) + ' < ' + str(dumpfile)
cmdline = 'ssh -t -o LogLevel=Error ' + str(src) + ' ' + str(cmd)
print runCmd(cmdline)


